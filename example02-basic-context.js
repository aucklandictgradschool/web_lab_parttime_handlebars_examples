// This is a completely minimal Express / Handlebars application which sets everything up, and allows for "public" files to be served.

// Allow the use of Express and FS libraries
var express = require('express');
var fs = require('fs');

// Setup Express
var app = express();
app.set('port', process.env.PORT || 3000);

// Setup the Handlebars layout engine
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'example02/example02-main' }));
app.set('view engine', 'handlebars');

// When we recieve a GET request to "/", render the example02/example02-home layout.
// That layout will be provided with the given context
app.get('/', function (req, res) {

    // Some data to display in the browser
    var person = {
        name: "Thomas T-Rex",
        address: "123 Some Street",
        phNumber: "021 123 4567"
    };

    // The context object to pass to Handlebars
    var context = {
        thePerson: person,
        message: "This is a <strong>message</strong>!"
    };

    // Render the view, providing the context where we can get our data from
    res.render('example02/example02-home', context);
});

// Allow for access to files in the ./public directory
app.use(express.static(__dirname + "/public"));

// Start the server running
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});
