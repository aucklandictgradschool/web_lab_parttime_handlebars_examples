
// Allow us to use the Express framework
var express = require('express');

// Setup a new Express app
var app = express();

// The app should listen on port 3000, unless a different
// port is specified in the environment.
app.set('port', process.env.PORT || 3000);

// Specify that the app should use handlebars, and set handlebars as the app's view engine.
// Also specify that the default layout is the example01-main.handlebars file, located in the ./views/layouts/example01 directory.
var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'example01/example01-main' }));
app.set('view engine', 'handlebars');

// Specify that when we browse to "/" with a GET request, render the example01-home.handlebars file, located in the ./views/example01 directory.
app.get('/', function (req, res) {

    res.render('example01/example01-home');
});

// Specify that when we browse to "/nolayout" with a GET request, render the example01-nolayout.handlebars file, located in the ./views/example01 directory.
// Also specify that we should NOT use the default layout for this file.
app.get('/nolayout', function (req, res) {

    res.render('example01/example01-nolayout', {layout: false} );
});

/* We haven't specified any other routes, so browsing anywhere other than "/" will result in a default error page being returned. */

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});